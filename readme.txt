GroupNo : 28
Student-1: (Shrey Rajesh, 140050018)
Student-2: (A Hemanth Kumar, 140050036)
Student-3: (Sahith Thallapally, 140050063)

Honor Code:
		I pledge on my honor that I have not given or received any unauthorized assistance on this assignment or any previous task.

Signed by:
		Shrey Rajesh, A Hemanth Kumar, Sahith Thallapally

Contributions:
		Student-1: 100%
		Student-2: 100%
		Student-3: 100%

Citations:
https://docs.djangoproject.com/en/1.8/intro/
http://www.tangowithdjango.com/book17/
https://docs.djangoproject.com/en/dev/howto/static-files/
https://docs.djangoproject.com/en/1.8/topics/auth/default/
http://getbootstrap.com/components/
http://www.w3schools.com/bootstrap/bootstrap_forms_inputs.asp
http://www.w3schools.com/bootstrap/bootstrap_forms_inputs.asp
http://stackoverflow.com/questions/16517718/bootstrap-number-validation
http://stackoverflow.com/questions/7547644/pass-variable-in-document-getelementbyid-in-javascript
http://stackoverflow.com/questions/21579532/show-hide-select-options-based-on-previous-selection-dropdown-in-jquery-or-javas
http://stackoverflow.com/questions/8454510/open-url-in-same-window-and-in-same-tab
https://www.youtube.com/watch?v=CFypO_LNmcc
http://stackoverflow.com/questions/17111046/using-django-templates-to-set-default-values
http://stackoverflow.com/questions/3518002/how-can-i-set-the-default-value-for-an-html-select-element
http://stackoverflow.com/questions/19863644/in-django-how-to-pass-selected-dropdown-value-from-template-to-view
http://stackoverflow.com/questions/3732106/django-template-variable-value-to-string-literal-comparison-fails
http://stackoverflow.com/questions/29725932/deleting-rows-with-python-in-a-csv-file
http://stackoverflow.com/questions/15128135/setting-debug-false-causes-500-error
http://stackoverflow.com/questions/19926641/how-to-disable-back-button-in-browser-using-javascript
http://stackoverflow.com/questions/1156246/having-django-serve-downloadable-files
http://stackoverflow.com/questions/18266543/how-can-i-get-file-name-from-request-file-in-django
http://stackoverflow.com/questions/22831576/django-raises-multivaluedictkeyerror-in-file-upload
http://stackoverflow.com/questions/19655975/check-if-an-array-contains-duplicate-values
http://stackoverflow.com/questions/14672153/document-getelementbyid-value-returning-undefined
http://stackoverflow.com/questions/15924751/check-if-a-element-is-displaynone-or-block-on-click-jquery
http://www.w3schools.com/jsref/jsref_number.asp
http://www.w3schools.com/jsref/jsref_substring.asp
http://stackoverflow.com/questions/29588808/django-how-to-check-if-username-already-exists
http://www.w3schools.com/bootstrap/bootstrap_modal.asp

Comments:
