from django.shortcuts import render
from django.http import HttpResponse
from django.contrib import auth
from django.contrib.auth.models import User
from django.core.context_processors import csrf
from django import template
from django.views.static import serve
import csv
import os
from algo import algorithm

def index(request):
    return render(request, 'allocation/index.html')

def login(request):
    if request.method == "POST":
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        invalid = "false";
        if user is not None:
            if username == "admin":
                return render (request, 'allocation/admin.html')

            else:
                auth.login(request, user)
                c = {}
                c.update(csrf(request))
                uname = ""
                ucurr = ""
                ucpi = ""
                ucat = ""
                uroll = User.objects.get(username=username).first_name

                with open('templates/allocation/input_options.csv', 'a+') as csvfile:
                    spamreader = csv.reader(csvfile, delimiter=',')
                    for row in spamreader:
                        if row[0] == uroll:
                            branch=[None]*15
                            uname = row[1]
                            ucurr = row[2]
                            ucpi = row[3]
                            ucat = row[4]   
                            for j in range(15):
                                branch[j]=row[5+j]

                c.update({"pre_name": uname})
                c.update({"pre_roll": uroll})
                c.update({"pre_curr": ucurr})
                c.update({"pre_cpi": ucpi})
                c.update({"pre_cat": ucat})
                for x in range(15):
                    c.update({"pre_pref_"+str(x+1): branch[x]})
                ############################
                #### load details here #####
                ############################
                return render(request, 'allocation/login.html',c)
        else :
            c = {}
            c.update(csrf(request))
            c.update({"invalid": "true"})
            return render (request, 'allocation/index.html', c)
    else:
        return render(request, 'allocation/wrong.html')

def confirm(request):
    if request.method == "POST":

        iroll = request.user.first_name
        iname = request.POST.get('name')
        icpi = request.POST.get('cpi')
        icat = request.POST.get('cat') 
        currbranch = request.POST.get('branch')
        icurr = currbranch

        prefbranch=[]
        for i in range(1,15):
            prefbranch.append(request.POST.get("pref_"+str(i)))

        #var choices = prefbranch.length;
        submit = True

        c = {}
        c.update(csrf(request))
        cont = {}
        cont.update(csrf(request))
        if currbranch=="Your Branch" or icat=="Category" or iname == "":
            submit=False
            c.update({"curr": "true"})

        for i in range(0,14):
            if currbranch == prefbranch[i] :
                submit=False
                c.update({"same": "true"})
                break

        try:
            first = prefbranch.index("Select Branch")
        except ValueError:
            pass

        #if this condition is true implies duplicates exist
        if len(prefbranch[:first])!=len(set(prefbranch[:first])) :
            submit=False
            c.update({"twosame": "true"})

        if first==0:
            submit=False
            c.update({"noSel": "true"})

        for i in range(first + 1, 14):
            if prefbranch[i]!="Select Branch":
                submit=False
                c.update({"invPref": "true"})
                break
    

        if len(icpi) != 5 :
            submit=False
            c.update({"invCPI": "true"})

        else:
            if icpi[2] != "." :
                submit=False
                c.update({"invCPI": "true"})
            else:
                if icpi[:2].isdigit() and icpi[3:].isdigit() :
                    if float(icpi) < 0.00 or float(icpi) > 10.00 :
                        submit=False
                        c.update({"invCPI": "true"})
                    if ((icat=="Gen" or icat=="OBC") and float(icpi) < 8.00) or float(icpi) < 7.00 :
                        cont.update({"cpiLim": "true"})

                else:
                    submit=False
                    c.update({"invCPI": "true"})

        if submit == True:
            data=[]
            header=[None]*20
            header[0]=(iroll)
            header[1]=(iname)
            header[2]=(icurr)
            header[3]=(icpi)
            header[4]=(icat)
            for j in range(0,first):
                header[5+j]=(prefbranch[j]) 

            with open("templates/allocation/input_options.csv","r") as readfile:
                readCSV=csv.reader(readfile)
                for row in readCSV:
                    if row[0] != iroll:
                        data.append(row)
            readfile.close()

            with open("templates/allocation/input_options.csv","w") as workfile:
                writeCSV=csv.writer(workfile)
                for row in data:
                    writeCSV.writerow(row)
                writeCSV.writerow(header)
            workfile.close()

            ############################
            #### store details here ####
            ############################
            return render(request, 'allocation/confirm.html', cont)
        else:
            
            c.update({"pre_name": iname})
            c.update({"pre_roll": iroll})
            c.update({"pre_curr": icurr})
            c.update({"pre_cpi": icpi})
            c.update({"pre_cat": icat})
            for x in range(14):
                c.update({"pre_pref_"+str(x+1): prefbranch[x]})
            return render(request, 'allocation/login.html',c)
    else:
        return render(request, 'allocation/wrong.html')

def adduser(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        roll = request.POST.get('roll')

        if len(username) == 0 or len(password) == 0:
            c = {}
            c.update(csrf(request))
            c.update({"blank": "true"})
            return render (request, 'allocation/createuser.html', c)

        with open("templates/allocation/input_options.csv","r") as readfile:
            readCSV=csv.reader(readfile)
            for row in readCSV:
                if row[0] == roll:
                    c = {}
                    c.update(csrf(request))
                    c.update({"exists": "true"})
                    return render (request, 'allocation/createuser.html', c)

        if len(roll) != 9 or roll[0:2] != "15":
            c = {}
            c.update(csrf(request))
            c.update({"ROLL": "true"})
            return render (request, 'allocation/createuser.html', c)

        if User.objects.filter(username=username).exists():
            c = {}
            c.update(csrf(request))
            c.update({"user": "true"})
            return render (request, 'allocation/createuser.html', c)

        else:
            user = User.objects.create_user(username=username,first_name=roll, password=password)
            with open("templates/allocation/input_options.csv","a+") as workfile:
                writeCSV=csv.writer(workfile)
                array=[None]*20
                array[0]=roll  
                writeCSV.writerow(array)
            workfile.close()
            return render(request, 'allocation/index.html')
            
    else:
        return render(request, 'allocation/wrong.html')

def createuser(request):
    return render(request, 'allocation/createuser.html')

def result(request):
    filepath = 'templates/allocation/input_options.csv'
    return serve(request, os.path.basename(filepath), os.path.dirname(filepath))

def upload(request):
    if request.method == 'POST':
        if 'file1' in request.FILES:
            progFile = request.FILES['file1']
            progFile.open("r")
            arr = progFile.readlines()
            progFile.close()
            
            openfile=open("templates/allocation/input_options.csv",'w')
            for row in arr:
                openfile.write(row)
            openfile.close()

        else:
            c = {}
            c.update(csrf(request))
            c.update({"file_1": "true"})
            return render(request, 'allocation/admin.html',c)

        if 'file2' in request.FILES:
            progFile = request.FILES['file2']
            progFile.open("r")
            arr = progFile.readlines()
            progFile.close()
            
            openfile=open("templates/allocation/input_programmes.csv",'w')
            for row in arr:
                openfile.write(row)
            openfile.close()
        else:
            c = {}
            c.update(csrf(request))
            c.update({"file_2": "true"})
            return render(request, 'allocation/admin.html',c)

        algorithm()
        filepath = 'templates/allocation/output.csv'
        return serve(request, os.path.basename(filepath), os.path.dirname(filepath))
    else:
        return render(request, 'allocation/wrong.html')
