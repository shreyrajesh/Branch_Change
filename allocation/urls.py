from django.conf.urls import patterns, url
from allocation import views

urlpatterns = patterns('',
        url(r'^$', views.index, name='index'),
        url(r'^index/', views.index, name='index'),
        url(r'^login/$', views.login, name='login'),
        url(r'^confirm/$', views.confirm, name='confirm'),
        url(r'^adduser/$', views.adduser, name='adduser'),
        url(r'^createuser/$', views.createuser, name='createuser'),
        url(r'^result/$', views.result, name='result'),
        url(r'^upload/$', views.upload, name='upload'),
        )