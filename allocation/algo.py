import csv

def algorithm():
	prog=[]
	#prog stores the branch strengths 
	hashtable={}
	#hashtable is a map from branches to integer to access any branch details
	i=0
	ineligible=[]
	with open("templates/allocation/input_programmes.csv","r") as readfile:
		readCSV=csv.reader(readfile, delimiter=',')
		for row in readCSV:
			hashtable[row[0]]=i
			temp=row
			temp[1]=int(row[1])
			temp[2]=int(row[2])
			temp.append(int(round(temp[1]*1.1))) 
			# temp[3] gives maximum strength of that branch
			temp.append(int(round(temp[1]*0.75)))
			# temp[4] gives minimum strength of that branch
			prog.append(temp)
			i=i+1
	readfile.close()

	cutoff=[None]*(i)
	# cutoff stores the list of students declined for each branch
	cpi=[None]*(i)
	# cpi stores the list of students declined for each branch
	mincpi=[]
	# mincpi stores the least cpi of student who has branchchanged from that branch
	for j in range(i):
		cutoff[j]=[0.00]
		cpi[j]=[12.00]
		mincpi.append(12.00)
	options=[]
	#options stores the students data who applied for branch change

	with open("templates/allocation/input_options.csv","r") as readfile:
		readCSV=csv.reader(readfile, delimiter=',')
		for row in readCSV:
			kaay=[""]*20
			i=0
			for elem in row:
				kaay[i]=elem
				i+=1
			kaay[3]=float(row[3])
			kaay.append(True)
			#True is row[20]
			kaay.append(row[2])
			# row[21] is current allotted branch
			if kaay[3] >= 8.00 :
				options.append(kaay)
			else : 
				if kaay[3] >= 7.00 and kaay[4]!="GE" and kaay[4]!="OBC" : 
					options.append(kaay)
				else:
					ineligible.append(kaay)
	readfile.close()
	options.sort(key=lambda x: -x[3])
	i=0
	allotted=[]
	# stores the alloted details of students
	hashmap={}
	#hashmap is a map from roll nos to integers 
	for row in options :
		hashmap[row[0]]=i
		temp=[None]*4
		temp[0]=row[0]
		temp[1]=row[1]
		temp[2]=row[2]
		temp[3]="Branch Unchanged"
		allotted.append(temp)
		i=i+1

	for row in ineligible:
		temp=[None]*4
		temp[0]=row[0]
		temp[1]=row[1]
		temp[2]=row[2]
		temp[3]="Ineligible"
		allotted.append(temp)
	updated=True
	while (updated) :
		updated=False
		for row in options :
			if row[20] : 
				if row[3] >= 9.00 :
					try:
						currb_index=row[5:].index(row[21])
						currb_index+=5
					except ValueError:
						pass
					for i in range(5,20) :
						if row[i]!="" :
							if prog[hashtable[row[i]]][3] > prog[hashtable[row[i]]][2] and cutoff[hashtable[row[i]]][0] <= row[3] and i<currb_index:
								#got his first preference
								prog[hashtable[row[i]]][2]+=1
								prog[hashtable[row[21]]][2]-=1
								updated=True
								if i==5:
									row[20]=False
								allotted[hashmap[row[0]]][3]=row[i]
								if row[21]!=row[2]:
									try:
									    cpi[hashtable[row[21]]].remove(row[3])
									except ValueError:
									    pass
								cpi[hashtable[row[i]]].append(row[3])
								cpi[hashtable[row[i]]].sort()
								if row[21]==row[2]:
									mincpi[hashtable[row[21]]]=min(mincpi[hashtable[row[21]]],row[3])
								row[21]=row[i]
								try:
								    cutoff[hashtable[row[i]]].remove(row[3])
								except ValueError:
								    pass
								break
							else : 
								cutoff[hashtable[row[i]]].append(row[3])
								cutoff[hashtable[row[i]]].sort(reverse=True)

				else :
					try:
						currbl_index=row[5:].index(row[21])
						currbl_index+=5
					except ValueError:
						pass

					
					for i in range(5,20) :
						if row[i]!="" :
							#ERROR i->21
							if prog[hashtable[row[i]]][3] > prog[hashtable[row[i]]][2] and prog[hashtable[row[21]]][4] <= (prog[hashtable[row[21]]][2]-1) and cutoff[hashtable[row[i]]][0] <= row[3] and i<currbl_index:
								#got his first preference
								prog[hashtable[row[i]]][2]+=1
								prog[hashtable[row[21]]][2]-=1
								updated=True
								if i==5:
									row[20]=False
								allotted[hashmap[row[0]]][3]=row[i]
								if row[21]!=row[2]:
									try:
									    cpi[hashtable[row[21]]].remove(row[3])
									except ValueError:
									    pass
								cpi[hashtable[row[i]]].append(row[3])
								cpi[hashtable[row[i]]].sort()
								if row[21]==row[2]:
									mincpi[hashtable[row[21]]]=min(mincpi[hashtable[row[21]]],row[3])
								row[21]=row[i]
								try:
								    cutoff[hashtable[row[i]]].remove(row[3])
								except ValueError:
								    pass
								break
							else : 
								cutoff[hashtable[row[i]]].append(row[3])
								cutoff[hashtable[row[i]]].sort(reverse=True)

		for row in options :
			if row[20]:
				if row[3]>=9.00:
					try:
						currbc_index=row[5:].index(row[21])
						currbc_index+=5
					except ValueError:
						pass
					# curr_branch=row[21]
					# p=row.index(curr_branch)# error here if he still is in his original branch
					for k in range(5,20):
						if row[k]!="":
							if cpi[hashtable[row[k]]][0]==row[3] and k<currbc_index:
								prog[hashtable[row[k]]][2]+=1
								prog[hashtable[row[21]]][2]-=1
								updated=True
								if k==5:
									row[20]=False
								allotted[hashmap[row[0]]][3]=row[k]
								if row[21]!=row[2]:
									try:
									    cpi[hashtable[row[21]]].remove(row[3])
									except ValueError:
									    pass
								cpi[hashtable[row[k]]].append(row[3])
								cpi[hashtable[row[k]]].sort()
								if row[21]==row[2]:
									mincpi[hashtable[row[21]]]=min(mincpi[hashtable[row[21]]],row[3])
								row[21]=row[k]
								try:
								    cutoff[hashtable[row[k]]].remove(row[3])
								except ValueError:
								    pass
								break
							else :
								cutoff[hashtable[row[k]]].append(row[3])
								cutoff[hashtable[row[k]]].sort(reverse=True)
				else :
					try:
						currbcl_index=row[5:].index(row[21])
						currbcl_index+=5
					except ValueError:
						pass
					# curr_branch=row[21]
					# p=row.index(curr_branch)# error here if he still is in his original branch
					for k in range(5,20):
						if row[k]!="":
							if cpi[hashtable[row[k]]][0]==row[3] and k<currbcl_index:
								prog[hashtable[row[k]]][2]+=1
								prog[hashtable[row[21]]][2]-=1
								updated=True
								if k==5:
									row[20]=False
								allotted[hashmap[row[0]]][3]=row[k]
								if row[21]!=row[2]:
									try:
									    cpi[hashtable[row[21]]].remove(row[3])
									except ValueError:
									    pass
								cpi[hashtable[row[k]]].append(row[3])
								cpi[hashtable[row[k]]].sort()
								if row[21]==row[2]:
									mincpi[hashtable[row[21]]]=min(mincpi[hashtable[row[21]]],row[3])
								row[21]=row[k]
								try:
								    cutoff[hashtable[row[k]]].remove(row[3])
								except ValueError:
								    pass
								break
							else :
								cutoff[hashtable[row[k]]].append(row[3])
								cutoff[hashtable[row[k]]].sort(reverse=True)

							if mincpi[hashtable[row[21]]]==row[3] and prog[hashtable[row[k]]][3] > prog[hashtable[row[k]]][2] and row[21]==row[2] and k<currbcl_index:
								prog[hashtable[row[k]]][2]+=1
								prog[hashtable[row[21]]][2]-=1
								updated=True
								if k==5:
									row[20]=False
								allotted[hashmap[row[0]]][3]=row[k]
								if row[21]!=row[2]:
									try:
									    cpi[hashtable[row[21]]].remove(row[3])
									except ValueError:
									    pass
								cpi[hashtable[row[k]]].append(row[3])
								cpi[hashtable[row[k]]].sort()
								row[21]=row[k]
								try:
								    cutoff[hashtable[row[k]]].remove(row[3])
								except ValueError:
								    pass
								break
							else:
								cutoff[hashtable[row[k]]].append(row[3])
								cutoff[hashtable[row[k]]].sort(reverse=True)


	allotted.sort(key=lambda x: x[0])

	with open("templates/allocation/output.csv","w") as workfile:
		writeCSV=csv.writer(workfile)
		for row in allotted :
			writeCSV.writerow(row)
	workfile.close()

	header=["Branch","Sanctioned","current","maximum","minimum"]
	with open("templates/allocation/stats.csv","w") as workfile:
		writeCSV=csv.writer(workfile)
		writeCSV.writerow(header)
		for row in prog :
			writeCSV.writerow(row)
	workfile.close()


